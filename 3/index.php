<?php
require_once 'arrays.php';
?>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Поиграем с городами</title>
    <style>
        body {
            font-family: sans-serif;
        }

    </style> 
</head>
<body>

    <h1>Массив регионов и городов</h1>

    <?php
    foreach ($mainArray as $region => $city)
    {
        echo  "<h3>$region</h3>";
        foreach ($city as $param => $pp)
        {
            echo  "$pp<br>";
        }
    }
    ?>

    <h1>Список объединенных городов</h1>
    <?php

    $arr=array();
    foreach ($mainArray as $region => $arCity)
    {
        foreach ($arCity as $ind => $City)
        {
            $ar = explode(" ",$City);
            if(count($ar)==2)
                $arr[$ar[0]] = $ar[1];
            $ar = explode("-",$City);
            if(count($ar)==2)
                $arr[$ar[0]] = $ar[1];
        }
    }

    $keys = array_keys($arr);
    $values = array_values($arr);
    shuffle($keys);
    shuffle($values);
    $cnt = count($keys)-1;

    for($i=0;$i<=$cnt;$i++)
    {
        echo $keys[$i]." ". $values[$i]."<br>";
    }

    ?>

</body>
</html>