<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Угадай число</title>
    <style>
        body {
            font-family: sans-serif;
            text-align: center;
        }
    </style>
</head>
<body>
<?php
$secret = 83;
$begin = "<a href='index.php'>Попробовать снова</a>"; 

if (isset($_REQUEST['numb']))  {

    $number = $_REQUEST['numb'];
    if($number < $secret) {
        echo "<h1>Слишком мало :(</h1>";
        echo $begin;
    } elseif ($number > $secret) {
        echo "<h1>Слишком много :(</h1>";
        echo $begin;
    } else {
        header("Location: success.php");
    }
}
?>
</body>
</html>
