<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Страница пользователя Владислав Дробь</title>
    <style>
        body {
            font-family: sans-serif;
        }

        dl {
            display: table-row;
        }

        dt, dd {
            display: table-cell;
            padding: 5px 10px;
        }
    </style>
</head>
<body>

<?php
// Configs
$firstName = "Владислав";
$lastName = "Дробь";
$yearsOld = "32";
$email = "vlad@drob.kz";
$city = "Алматы";
$desc = "Руководитель интернет магазина";
$siteName = "\"КОРС\"";
$siteURL = "www.kors.kz";
?>

<h1>Страница пользователя <?php echo $firstName." ".$lastName;?></h1>
<dl>
    <dt>Имя</dt>
    <dd><?php echo $firstName;?></dd>
</dl>
<dl>
    <dt>Возраст</dt>
    <dd><?php echo $yearsOld;?></dd>
</dl>
<dl>
    <dt>Адрес электронной почты</dt>
    <dd><a href="mailto:<?php echo $email;?>"><?php echo $email;?></a></dd>
</dl>
<dl>
    <dt>Город</dt>
    <dd><?php echo $city;?></dd>
</dl>
<dl>
    <dt>О себе</dt>
    <dd><?php echo $desc;?> - <a href="http://<?php echo $siteURL;?>"><?php echo $siteName;?></a></dd>
</dl>

</body>
</html>
