<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Домашнее задание Netology </title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

<h1 align="center">Домашние задания по теме: <br>"PHP/SQL: back-end разработка и базы данных"</h1>
<h2 align="center">Слушатель курса: Владислав Дробь</h2>

<div class="container">
    <section class="acc-container">
        <!-- 1 пункт -->
        <div>
            <input id="acc-1" name="accordion-1" type="checkbox" />
            <label for="acc-1">1. Синтаксис PHP</label>
            <article class="acc-small">
                <ul>
                        <li><a href="me/about.php">Домашнее задание к лекции 1.1 Введение в PHP</a></li>
                        <li><a href="2/">Домашнее задание к лекции 1.2 Основы PHP</a></li>
                        <li><a href="3/">Домашнее задание к лекции 1.3 Строки, массивы и объекты</a></li>
                        <li><a href="4/">Домашнее задание к лекции 1.4 Стандартные функции</a></li>
                </ul>
            </article>
        </div>
        <!-- 2 пункт -->
        <div>
            <input id="acc-2" name="accordion-1" type="checkbox" />
            <label for="acc-2">2. PHP на веб-сервере</label>
            <article class="acc-small">
                <ul>
                    <li><a href="5/">Домашнее задание к лекции 2.1 Установка и настройка веб-сервера</a></li>
                    <li><a href="#">Домашнее задание к лекции 2.2 Обработка форм</a></li>
                    <li><a href="#">Домашнее задание к лекции 2.3 PHP и HTTP</a></li>
                    <li><a href="#">Домашнее задание к лекции 2.4 Куки, сессии и авторизация</a></li>
                </ul>
            </article>
        </div>
        <!-- 3 пункт -->
        <div>
            <input id="acc-3" name="accordion-1" type="checkbox" />
            <label for="acc-3">3. Объектно-ориентированное программирование</label>
            <article class="acc-small">
                <ul>
                    <li><a href="#">Домашнее задание к лекции 3.1 Классы и объекты</a></li>
                    <li><a href="#">Домашнее задание к лекции 3.2 Наследование и интерфейсы</a></li>
                    <li><a href="#">Домашнее задание к лекции 3.3 Пространства имен, перегрузка и встроенные интерфейсы и классы</a></li>
                </ul>
            </article>
        </div>
        <!-- 4 пункт -->
        <div>
            <input id="acc-4" name="accordion-1" type="checkbox" />
            <label for="acc-4">4. MySQL</label>
            <article class="acc-small">
                <ul>
                    <li><a href="#">Домашнее задание к лекции 4.1 Реляционные базы данных и SQL</a></li>
                    <li><a href="#">Домашнее задание к лекции 4.2 Запросы SELECT, INSERT, UPDATE и DELETE</a></li>
                    <li><a href="#">Домашнее задание к лекции 4.3 SELECT из нескольких таблиц</a></li>
                    <li><a href="#">Домашнее задание к лекции 4.4 Управление таблицами и базами данных</a></li>
                </ul>
            </article>
        </div>
        <!-- 5 пункт -->
        <div>
            <input id="acc-5" name="accordion-1" type="checkbox" />
            <label for="acc-5">5. Обзор фреймфорков</label>
            <article class="acc-small">
                <ul>
                    <li><a href="#">Домашнее задание к лекции 5.1 Шаблонизатор (Smarty, Twig или другой)</a></li>
                    <li><a href="#">Домашнее задание к лекции 5.2 Фреймфорк (Zend, Yii или другой)</a></li>
                </ul>
            </article>
        </div>
    </section>
</div>

</body>
</html>