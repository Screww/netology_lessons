<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Задание к занятию «Стандартные функции»</title>
    <style>
        body {
            font-family: sans-serif;
        }
        table {
            border:0;
        }
        td {
            padding: 10px;
            border-bottom: dotted 1px;
        }
    </style>
</head>
<body>

<h1>Задание к занятию «Установка и настройка веб-сервера»</h1>

Домашнее задание следующее:<br>

<p>Необходимо вывести таблицу из CSV файла в виде HTML таблицы.<br>
CSV файл, где колонки разделены “;”</p>

<h1>Решение:</h1>
<?php

$data = File("data.csv"); // путь к CSV файлу

echo "<table><tr>";

$dat_arr = explode(";", $data[0]); // отбираем первую строку как заголовок

for ($n=0;$n<count($dat_arr);$n++)
{
    echo "<td><b>$dat_arr[$n]</b></td>";
}
echo "</tr>";

for ($i=1;$i<count($data);$i++)
{
    $data_array = explode(";", $data[$i]);
    echo "<tr>";
    for ($f=0;$f<count($data_array);$f++)
    {
        echo "<td><i>$data_array[$f]</i></td>";
    }
    echo "</tr>";
}
echo "</table>";
?>

<p><a href="data.csv">Скачать CSV файл</a></p>

<p><a href="http://university.netology.ru/u/drob/"><<< К списку заданий</a></p>


</body>
</html>